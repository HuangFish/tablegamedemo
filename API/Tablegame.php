<?php

if(!class_exists('link_db')) require_once('db_connection.php');
//檔名跟class有一致
class Tablegame extends Control implements RESTfulInterface {
    function restPost($segments) {
		$db = link_db::getIntance();
		if ( empty($segments) ) { // Without parameter
			if(!isset($_POST["search_data"])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "SELECT A.* ,C.id category_id, C.name category_name FROM `tablegame` as A left join `tablegame_category` as B on A.id=B.tablegame_id left join `category` as C on C.id=B.category_id ";
			$cmd .="where A.`name` like '%".$_POST["search_data"]."%'";
			$cmd .= " or A.`simple` like '%".$_POST["search_data"]."%'";
			$cmd .= " or A.`introduce` like '%".$_POST["search_data"]."%'";
			$cmd .= " or A.`player` like '%".$_POST["search_data"]."%'";
			$cmd .= " or A.`playtime` like '%".$_POST["search_data"]."%'";
			$cmd .= " ORDER BY A.`id`";
			if($db->getNum_rows($cmd)){
				$output = $db->getAll($cmd);
				$pre_id = 0; //先初始化
				$tg_data = array(); //先初始化
				foreach($output as $key=>$value){
					if($pre_id != $value["id"]){
						//將資料倒入正確的模型
						$now_key = count($tg_data);
						foreach($value as $data_key=>$data_value){
							if($data_key == "updated_at"){
								$tg_data[$now_key]["category"] = array(); //category先初始化
								if(!empty($value["category_id"])) 
									$tg_data[$now_key]["category"][] = array("id"=>$value["category_id"],"name"=>$value["category_name"]);
							}elseif($data_key == "category_id") break;
							$tg_data[$now_key][$data_key] = $data_value;
						}
						$pre_id = $value["id"]; //記錄上一個id
						$pre_key = $now_key; //記錄上一個儲存點
					}else{ //上一個id與這個相同->分類有異
						//新增分類進去
						$tg_data[$pre_key]["category"][] = array("id"=>$value["category_id"],"name"=>$value["category_name"]);
					}
				}
				//$cmd1 = "SELECT A.tablegame_id,A.category_id,B.name FROM `tablegame_category` as A left join `category` as B on A.category_id = B.id ";
				//if($db->getNum_rows($cmd1))	$output1 = $db->getAll($cmd1);
				//print_r($output1);
				
				//$output[0]["category"] = array(array("id"=>"1","name"=>"反應"),array("id"=>"2","name"=>"心機"));
				//print_r($output);
				self::AcceptAPI($tg_data);
				//echo urldecode(json_encode($tg_data));
				//echo urldecode(json_encode(array('Header'=>array('Status'=>true,),'Body'=>$output)));
			}else self::exceptionResponse(404, 'Not found');
        }elseif($segments[0] == "new"){
			if(empty($_POST["category"]) || empty($_POST["name"])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "INSERT INTO `tablegame`(`category`, `name`, `simple`, `introduce`, `player`, `playtime`, `photo`, `updated_at`, `created_time`) VALUES ";
			$cmd .= "('".$_POST["category"]."','".$_POST["name"]."','".$_POST["simple"]."','".$_POST["introduce"]."','".$_POST["player"]."','".$_POST["playtime"]."','".$_POST["photo"]."',now(),now())";
			if(!$db->query($cmd)) self::exceptionResponse(501, 'Not Implemented');
			else self::AcceptAPI("created success");
		}else self::exceptionResponse(405, 'Method Not Allowed');
    }

    function restGet($segments) {
		$db = link_db::getIntance();
		if ( empty($segments) ) { // Without parameter
			$cmd = "SELECT A.* ,C.id category_id, C.name category_name FROM `tablegame` as A left join `tablegame_category` as B on A.id=B.tablegame_id left join `category` as C on C.id=B.category_id ORDER BY A.`id`";
			//$cmd = "SELECT * FROM `tablegame` order by `id`";
			if($db->getNum_rows($cmd)){
				$output = $db->getAll($cmd);
				$pre_id = 0; //先初始化
				$tg_data = array(); //先初始化
				foreach($output as $key=>$value){
					if($pre_id != $value["id"]){
						//將資料倒入正確的模型
						$now_key = count($tg_data);
						foreach($value as $data_key=>$data_value){
							if($data_key == "updated_at"){
								$tg_data[$now_key]["category"] = array(); //category先初始化
								if(!empty($value["category_id"])) 
									$tg_data[$now_key]["category"][] = array("id"=>$value["category_id"],"name"=>$value["category_name"]);
							}elseif($data_key == "category_id") break;
							$tg_data[$now_key][$data_key] = $data_value;
						}
						$pre_id = $value["id"]; //記錄上一個id
						$pre_key = $now_key; //記錄上一個儲存點
					}else{ //上一個id與這個相同->分類有異
						//新增分類進去
						$tg_data[$pre_key]["category"][] = array("id"=>$value["category_id"],"name"=>$value["category_name"]);
					}
				}
				//$cmd1 = "SELECT A.tablegame_id,A.category_id,B.name FROM `tablegame_category` as A left join `category` as B on A.category_id = B.id ";
				//if($db->getNum_rows($cmd1))	$output1 = $db->getAll($cmd1);
				//print_r($output1);
				
				//$output[0]["category"] = array(array("id"=>"1","name"=>"反應"),array("id"=>"2","name"=>"心機"));
				//print_r($output);
				self::AcceptAPI($tg_data);
				//echo urldecode(json_encode($tg_data));
				//echo urldecode(json_encode(array('Header'=>array('Status'=>true,),'Body'=>$output)));
			}else self::exceptionResponse(404, 'Not found');
        }elseif(is_numeric($segments[0])){
			$cmd = "SELECT * FROM `tablegame` where `id` = '".$segments[0]."' ORDER BY `id`";
			if($db->getNum_rows($cmd)){
				$output = $db->getAll($cmd);
				self::AcceptAPI($output);
				//echo urldecode(json_encode($output));
			}else self::exceptionResponse(404, 'Not found');
		}else self::exceptionResponse(405, 'Method Not Allowed');
    }

    function restPut($segments) {
		$db = link_db::getIntance();
		if($segments[0] == "update"){
			//put僅能以file_get_contents取得，urldecode(json_encode))是解碼用的
			$put_data = urldecode(json_encode(file_get_contents('php://input'))); 
			//因為是字串所以做切割
			$token = strtok($put_data, "&"); 
			while ($token !== false){
				$put_token[] = $token;
				$token = strtok("&");
			}
			$put_token[count($put_token)-1] = substr($put_token[count($put_token)-1],0,-1); //去掉最後面那個變數後面的"
			//print_r($put_token);
			$pre_check = ""; //記錄上一個比對到的東西
			$checkstr = array("id","category","name","simple","introduce","player","playtime","photo","status"); //變數名稱集合
			//將切割出來的資料，正確的放到每個變數中
			foreach($put_token as $put_key => $put_value){
				$hit = 0; //有沒有對照到checkstr
				foreach($checkstr as $ck_key => $ck_value){
					if($domain = strstr($put_value, $ck_value)){
						//echo $put_value."->".$ck_value."\n";
						$token = strtok($domain, $ck_value."="); 
						$put[$ck_value] = $token;
						$pre_check = $ck_value;
						$hit = 1;
						//break;
						//echo $token;
						//echo $domain;
					}
				}
				if($hit == 0) $put[$pre_check] .= "&".$put_value; //沒有比對到，因為字串裡面有&被切割了
			}
			//print_r($put);
			if(empty($put["category"]) || empty($put["name"]) || empty($put["id"])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "UPDATE `tablegame` SET `category`='".$put["category"]."',`name`='".$put["name"]."',`simple`='".$put["simple"]."',`introduce`='".$put["introduce"]."'";
			$cmd .= ",`player`='".$put["player"]."',`playtime`='".$put["playtime"]."',`photo`='".$put["photo"]."',`status`='".$put["status"]."',`updated_at`=now() WHERE `id`='".$put["id"]."' ";
			if(!$db->query($cmd)) self::exceptionResponse(501, 'Not Implemented');
			else self::AcceptAPI("updated success");
		}else self::exceptionResponse(405, 'Method Not Allowed');
        //echo 'Update resource: ' . $segments[0];
        //echo '<br/> you put data: ' . file_get_contents('php://input'); // read the raw put data.
    }

    function restDelete($segments) {
		$db = link_db::getIntance();
		if($segments[0] == "delete"){ 
			if(!is_numeric($segments[1])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "DELETE FROM `tablegame` WHERE `id`=".$segments[1];
			if(!$db->query($cmd)) self::exceptionResponse(501, 'Not Implemented');
			else self::AcceptAPI("delete success");
		}else self::exceptionResponse(405, 'Method Not Allowed');
        //echo 'Delete resource: ' . $segments[0];
        //echo '<br/> you put data: ' . file_get_contents('php://input'); // read the raw put data.
    }
}
?>