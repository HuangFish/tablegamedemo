<?php

if(!class_exists('link_db')) require_once('db_connection.php');
//檔名跟class有一致
class User extends Control implements RESTfulInterface {
    function restPost($segments) {
		$db = link_db::getIntance();
		if ( empty($segments) ) { // Without parameter
			if(!isset($_POST["search_data"])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "SELECT * FROM `tablegame` where `name` like '%".$_POST["search_data"]."%'";
			$cmd .= " or `simple` like '%".$_POST["search_data"]."%'";
			$cmd .= " or `introduce` like '%".$_POST["search_data"]."%'";
			$cmd .= " or `player` like '%".$_POST["search_data"]."%'";
			$cmd .= " or `playtime` like '%".$_POST["search_data"]."%'";
			$cmd .= " ORDER BY `id`";
			if($db->getNum_rows($cmd)){
				$output = $db->getAll($cmd);
				self::AcceptAPI($output);
			}else self::exceptionResponse(404, 'Not found');
        }elseif($segments[0] == "new"){
			if(empty($_POST["category"]) || empty($_POST["name"])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "INSERT INTO `tablegame`(`category`, `name`, `simple`, `introduce`, `player`, `playtime`, `photo`, `updated_at`, `created_time`) VALUES ";
			$cmd .= "('".$_POST["category"]."','".$_POST["name"]."','".$_POST["simple"]."','".$_POST["introduce"]."','".$_POST["player"]."','".$_POST["playtime"]."','".$_POST["photo"]."',now(),now())";
			if(!$db->query($cmd)) self::exceptionResponse(501, 'Not Implemented');
			else self::AcceptAPI("created success");
		}else self::exceptionResponse(405, 'Method Not Allowed');
    }

    function restGet($segments) {
		$db = link_db::getIntance();
		if(empty($segments)) self::exceptionResponse(405, 'Method Not Allowed');
		elseif ( $segments[0] == "logout") { // Without parameter
			$cmd = "SELECT * FROM `tablegame` ORDER BY `id`";
			if($db->getNum_rows($cmd)){
				$output = $db->getAll($cmd);
				self::AcceptAPI($output);
			}else self::exceptionResponse(404, 'Not found');
        }elseif(is_numeric($segments[0])){
			$cmd = "SELECT * FROM `user` where `id` = '".$segments[0]."' ORDER BY `id`";
			if($db->getNum_rows($cmd)){
				$output = $db->getAll($cmd);
				self::AcceptAPI($output);
			}else self::exceptionResponse(404, 'Not found');
		}else self::exceptionResponse(405, 'Method Not Allowed');
    }

    function restPut($segments) {
		$db = link_db::getIntance();
		if($segments[0] == "update"){
			//put僅能以file_get_contents取得，urldecode(json_encode))是解碼用的
			$put_data = urldecode(json_encode(file_get_contents('php://input'))); 
			//因為是字串所以做切割
			$token = strtok($put_data, "&"); 
			while ($token !== false){
				$put_token[] = $token;
				$token = strtok("&");
			}
			$put_token[count($put_token)-1] = substr($put_token[count($put_token)-1],0,-1); //去掉最後面那個變數後面的"
			//print_r($put_token);
			$pre_check = ""; //記錄上一個比對到的東西
			$checkstr = array("id","name","email","phone","username","password","status"); //變數名稱集合
			//將切割出來的資料，正確的放到每個變數中
			foreach($put_token as $put_key => $put_value){
				$hit = 0; //有沒有對照到checkstr
				foreach($checkstr as $ck_key => $ck_value){
					if($domain = strstr($put_value, $ck_value)){
						//echo $put_value."->".$ck_value."\n";
						$token = strtok($domain, $ck_value."="); 
						$put[$ck_value] = $token;
						$pre_check = $ck_value;
						$hit = 1;
						//break;
						//echo $token;
						//echo $domain;
					}
				}
				if($hit == 0) $put[$pre_check] .= "&".$put_value; //沒有比對到，因為字串裡面有&被切割了
			}
			//print_r($put);
			if(empty(empty($put["name"]) || empty($put["id"]) || empty($put["username"]) || empty($put["password"]) || empty($put["status"])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "UPDATE `user` SET `name`='".$put["name"]."',`username`='".$put["username"]."',`password`='".$put["password"]."',`status`='".$put["status"]."'";
			$cmd .= ",`email`='".$put["email"]."',`phone`='".$put["phone"]."',`updated_at`=now() WHERE `id`='".$put["id"]."' ";
			if(!$db->query($cmd)) self::exceptionResponse(501, 'Not Implemented');
			else self::AcceptAPI("updated success");
		}else self::exceptionResponse(405, 'Method Not Allowed');
        //echo 'Update resource: ' . $segments[0];
        //echo '<br/> you put data: ' . file_get_contents('php://input'); // read the raw put data.
    }

    function restDelete($segments) {
		$db = link_db::getIntance();
		if($segments[0] == "delete"){ 
			if(!is_numeric($segments[1])) self::exceptionResponse(406, 'Not Acceptable');
			$cmd = "DELETE FROM `tablegame` WHERE `id`=".$segments[1];
			if(!$db->query($cmd)) self::exceptionResponse(501, 'Not Implemented');
			else self::AcceptAPI("delete success");
		}else self::exceptionResponse(405, 'Method Not Allowed');
        //echo 'Delete resource: ' . $segments[0];
        //echo '<br/> you put data: ' . file_get_contents('php://input'); // read the raw put data.
    }
}
?>